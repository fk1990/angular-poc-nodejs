import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent implements OnInit {
  title = 'formations';
  showSidebar : boolean = false;
  constructor(private auth : AuthService, private router : Router){}
  ngOnInit(){


  }
}
