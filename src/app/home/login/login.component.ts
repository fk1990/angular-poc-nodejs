import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm : FormGroup
  submitted : boolean = false;
  storage : any = null;
  failedError : string = null;
  constructor(private fb : FormBuilder, private auth : AuthService, private router : Router) { }

  ngOnInit() {
    if(this.auth.isLoggednIn){
      this.router.navigate(['/admin']);
    }
    this.loginForm = this.fb.group({
      email : ['', Validators.compose([Validators.required,Validators.email])],
      password : ['', Validators.required]
     }
    )

  }
  get form(){
    return this.loginForm.controls;
  }
  onLogin(value){
    if(value.email && value.password){
      this.submitted = false;
      this.auth.doLogin(value.email, value.password).subscribe((res)=>{
        this.storage = res;
        localStorage.setItem("id_token", this.storage.token);
        localStorage.setItem('id_admin',this.storage.adminId)
        this.router.navigate(['/admin']);
      },
      (err) =>{
          this.failedError = err.error;
      
      });
    }else{
      this.submitted = true;
      
    }
  }

}
