import { TestBed } from '@angular/core/testing';

import { FormationsService } from './formations.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('FormationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports : [HttpClientModule]
  }));

  it('should be created', () => {
    const service: FormationsService = TestBed.get(FormationsService);
    expect(service).toBeTruthy();
  });
});
