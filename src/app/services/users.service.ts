import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';
import { User } from '../administrator/users/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private methoS = new Subject<any>();
  headers = {
    headers : new HttpHeaders({
      'Content-Type':  'application/json',
    })
  }
  constructor(private http: HttpClient) { }

  allUsers() : Observable<User[]>{
    return this.http.get<User[]>(environment.baseUrl+"users");
}
  addUsers(user){
    //let headers = new Headers({ 'Content-Type': 'application/json' });  
    return this.http.post(environment.baseUrl+"users/add",user,this.headers);
  }

  detailUser(id): Observable<User>{
    return this.http.get<User>(environment.baseUrl+"users/"+id)
  }
  deleteUser(id){
    return this.http.delete(environment.baseUrl+"users/delete/"+ id)
  }
  updateUsers(user : User) : Observable<User>{
    return this.http.put<User>(environment.baseUrl+"users/edit/"+user.id_users, user,this.headers);
  }
  userByRow(from, to){
    return this.http.post(environment.baseUrl+"users/grouped", {from : from, to : to})

  }
 
}
