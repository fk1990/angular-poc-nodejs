import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router : Router) { }

  doLogin(email, password){
    return this.http.post(environment.baseUrl+"users/login", {email, password});
  }


  isLoggednIn() {
    if(localStorage.getItem('id_token')){
      return true;
    }else{
      return false;
    }

  }
  logout() {
    localStorage.removeItem("id_token");
    this.router.navigate(['/']);

  }
  getToken(){
    return localStorage.getItem('id_token');
  }
  getAdmin(id){
    return this.http.get(environment.baseUrl+"admins/detail/"+id)
  }
}
