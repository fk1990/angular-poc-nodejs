import { TestBed } from '@angular/core/testing';

import { SubscribesService } from './subscribes.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('SubscribesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports : [HttpClientModule]
  }));

  it('should be created', () => {
    const service: SubscribesService = TestBed.get(SubscribesService);
    expect(service).toBeTruthy();
  });
});
