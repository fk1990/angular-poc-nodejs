import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector : Injector){

  }
  intercept(req, next){
    let tokenService = this.injector.get(AuthService);
    let token = req.clone({
      setHeaders : {
        Authorization : `Bearer ${tokenService.getToken()}`
      }
    })
    return next.handle(token);
  }
 
}
