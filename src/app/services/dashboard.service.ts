import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private _http: HttpClient) { }

  dashUser(){
    return this._http.get(`${environment.baseUrl+"dashboard/users"}`).pipe(map((res)=> res))
  }
}
