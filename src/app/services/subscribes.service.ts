import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subscribe } from '../administrator/formationsUsers/subscribe';

@Injectable({
  providedIn: 'root'
})
export class SubscribesService {

  constructor(private http : HttpClient) { }

  getSubscribers(){
    return this.http.get<Subscribe[]>(environment.baseUrl+"subscribes");
  }
  getSubscriber(id){
    return this.http.get<Subscribe>(environment.baseUrl+"subscribes/"+id)
  }
  addSubscriber(user){
    return this.http.post(environment.baseUrl+'subscribes/add',user);

  }
}
