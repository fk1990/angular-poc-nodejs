import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Formation } from '../administrator/formations/Formation';

@Injectable({
  providedIn: 'root'
})
export class FormationsService {
  headers = {
    headers : new HttpHeaders({
      'Content-Type':  'application/json',
    })
  }
  constructor(private http : HttpClient) { }

  getAllFormations() : Observable<Formation[]>{
    return this.http.get<Formation[]>(environment.baseUrl+"formations")

  }
  getOneFormation(id) : Observable<Formation>{
    return this.http.get<Formation>(environment.baseUrl+"formations/"+id)
  }
  onDeleteFormation(id) {
    return this.http.delete(environment.baseUrl+"formations/delete/"+id)
  }
  onCreateFormation(formation): Observable<Formation>{
    return this.http.post<Formation>(environment.baseUrl+"formations/add",formation,this.headers);
  }
  onUpdateFormation(formation : Formation):Observable<any>{
    return this.http.put(environment.baseUrl+"formations/edit/"+ formation.id_formation,formation)
  }
  uploadFormationImages(file){
    return this.http.post(environment.baseUrl+"formations/file",file);
  }
  subscribeUser(subscribe){
    return this.http.post(environment.baseUrl+'formations/users',subscribe)
  }

}
