import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  title = new BehaviorSubject("default title");
  link = new BehaviorSubject("");
  constructor() { }

  setTitle(title : string){
    this.title.next(title);
  }

  setLink(link : string){
    this.link.next(link);
  }
}
