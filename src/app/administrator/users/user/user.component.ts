import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from '../user';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user : User;
  id : number;
  constructor(private detailUser : UsersService, private route : ActivatedRoute, private titleService : TitleService) {
    this.route.params.subscribe(params =>(this.id=params['id']))
   }

  ngOnInit() {
    this.detailUser.detailUser(this.id).subscribe((res) => {
      this.user = res;
      this.setTitleLink(this.user.first_name + " " + this.user.last_name,'users');
    });
    
  }

  setTitleLink(title, link){
    this.titleService.setTitle(title);
    this.titleService.setLink(link);
  }

}
