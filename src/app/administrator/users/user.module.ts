import { NgModule } from '@angular/core';
import { AllUsersComponent } from './all-users/all-users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserRoutingModule } from './user-routing.module';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FilterPipe } from '../layout/pipes/filter.pipe';



@NgModule({
  declarations: [
    AllUsersComponent,
    AddUserComponent,
    EditUserComponent,
    UserComponent,
    FilterPipe

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserRoutingModule,
    PaginationModule.forRoot(),
    FormsModule,


  ],
})


export class UserModule{

}