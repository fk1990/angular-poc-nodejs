export interface User {
    id_users: number;
    last_name: string;
    first_name: string;
    password : string;
    phone: number;
    website: string;
    poste:string;
    email:string;
}