import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { User } from '../user';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  formUser : FormGroup;
  submitted = false;
  formValid = false;
  id : number;
  @Input() user : User;
  expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  constructor(private fb : FormBuilder, private userService : UsersService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe((params)=> this.id = params['id']);
   }

  ngOnInit() {
    this.formUser = this.fb.group({
      last_name : ['',Validators.compose([Validators.required,Validators.minLength(3),Validators.pattern('^[a-zA-Z \-\']+')])],
      first_name : ['',Validators.compose([Validators.required,Validators.minLength(3)])],
      email : ['',Validators.compose([Validators.required,Validators.email])],
      password : ['',Validators.compose([Validators.required])],
      phone : ['',Validators.pattern("^[0-9]*$")],
      website: ['',Validators.pattern(this.expression)],
      poste : ['',Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])]
    })
  }
  get form(){
    return this.formUser.controls;
  }
  updateUser(user){
   // if(this.formUser.valid){
      this.userService.updateUsers(user).subscribe(
        (response) => {console.log("done ", response)},
);
      //this.formUser.reset();
    //}else{
      this.formValid = true;
      this.submitted = true;

    //}
  }
}
