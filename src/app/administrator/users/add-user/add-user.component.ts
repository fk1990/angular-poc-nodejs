import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { Subscription } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  formUser : FormGroup;
  submitted = false;
  formValid = false;

  expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  constructor(private fb  : FormBuilder,private add : UsersService, private alertService : AlertService, private headerService : TitleService) {
   }

  ngOnInit() {
    this.setTitleLink("Ajouter un utilisateur", "users");
    this.formUser = this.fb.group({
      nom : ['',Validators.compose([Validators.required,Validators.minLength(3),Validators.pattern('^[a-zA-Z \-\']+')])],
      prenom : ['',Validators.compose([Validators.required,Validators.minLength(3)])],
      email : ['',Validators.compose([Validators.required,Validators.email])],
      password : ['',Validators.compose([Validators.required])],
      phone : ['',Validators.pattern("^[0-9]*$")],
      website: ['',Validators.pattern(this.expression)],
      poste : ['',Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z \-\']+')])]
    })
  }
  get form(){
    return this.formUser.controls;
  }
  createUser(){
    if(this.formUser.valid){
      this.add.addUsers(this.formUser.value).subscribe((response) => {
        this.alertService.sendMessage("Utilisateur Ajouter Avec succès");
      });
      this.formUser.reset();
      
    }else{
      this.formValid = true;
      this.submitted = true;

    }
  }

  setTitleLink(title, link){
    this.headerService.setTitle(title);
    this.headerService.setLink(link)
  }

}
