import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteUserComponent } from './delete-user.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersService } from 'src/app/services/users.service';
import { ModalModule, BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

describe('DeleteUserComponent', () => {
  let component: DeleteUserComponent;
  let fixture: ComponentFixture<DeleteUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteUserComponent ],
      imports : [HttpClientModule, ModalModule.forRoot()],
      providers : [UsersService, BsModalRef, BsModalService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
