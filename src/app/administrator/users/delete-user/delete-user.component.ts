import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from '../user';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { SUBSTITUTION_EXPR_END } from '@angular/animations/browser/src/util';
import { subtract } from 'ngx-bootstrap/chronos/public_api';

@Component({
  selector: 'popup-delete-user',
  template: `
  <div class="modal-body text-center" *ngIf="id">
    <div class="alert alert-success" *ngIf="deleted">
      utilisateur supprimer avec succès
      
    </div>
  <p>Etes vous sur de supprimer un utilisateur </p>
  <button type="button" class="btn btn-default" (click)="confirm(id, index)" >Yes</button>
  <button type="button" class="btn btn-primary" (click)="decline()" >No</button>
</div>`,
})
export class DeleteUserComponent implements OnInit {
    deleted  = false;
    @Output()users = new EventEmitter<User[]>();
    @Output() useTest = new EventEmitter();
    id : number = null;
    data : any;
    count
  constructor(private deleteService : UsersService, public bsModalRed : BsModalRef, private router : Router, private ms : BsModalService) { }

  ngOnInit() {
  }

  confirm(id, i){
      this.count = this.count + 1;
    this.useTest.emit(this.useTest);
   /*this.deleteService.deleteUser(id).subscribe(
      (data) => {
        this.router.navigate(["admin/users"])

      }
   )*/
   
    this.bsModalRed.hide();
    
  }
  refreshUsers(){
    this.deleteService.allUsers().subscribe(
      data => {
        //this.users=data;
        console.log(this.users);
      }
      
    )
  }
  decline(){
    
    this.bsModalRed.hide()
  }
}
