import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { UsersService } from 'src/app/services/users.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/';

import { TitleService } from 'src/app/services/title.service';




@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss'],
})
export class AllUsersComponent implements OnInit {
  @Input() id;

  modalRef : BsModalRef;
  users: User[] ;
  userByTwelve : User[];
  length : number;
  constructor(private usersService : UsersService, private modal : BsModalService, private router : Router, private titleService : TitleService) { 
    
  }

  ngOnInit() {
    this.setTitle("Les utilisateurs");
    this.setLink("users/add");
    this.getAllUser();
  }

  // TITLE PAGE
  setTitle(title){
    this.titleService.setTitle(title);
  }
  getAllUser(){
    this.usersService.allUsers().subscribe(
      (data) =>{ 
        this.users = data ;
        this.userByTwelve =  this.users.slice(0,12);
        this.length = this.users.length;
      },
      (err) => {
        if (err instanceof HttpErrorResponse){
          if(err.status === 401){
            this.router.navigate(['/']);
          }
        }
      }
    )
  }

  deleteUser(id,i) {
    this.id = id;
    this.modalRef = this.modal.show(DeleteUserComponent,{initialState :{
      id : this.id,
      index : i

    }});
    this.modalRef.content.closeBtnName = 'Close';
  }
  pageChanged(event : PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.usersService.userByRow(startItem, endItem).subscribe( () => {
      this.userByTwelve = this.users.slice(startItem, endItem);
    })
    
  }
  setLink(link){
    this.titleService.setLink(link);
  }
}
