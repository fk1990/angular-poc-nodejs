import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllUsersComponent } from './all-users/all-users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UserComponent } from './user/user.component';
import { CommonModule } from '@angular/common';



const routes: Routes = [
      {path: "",component: AllUsersComponent},
      {path: "add",component: AddUserComponent},
      {path:"edit/:id",component:EditUserComponent},
      {path:"detail/:id", component:UserComponent},
];

@NgModule({
  imports: [CommonModule,RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
