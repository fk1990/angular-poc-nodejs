import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';
@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  title : string = "";
  link : string = "";
  constructor(private headerPage : TitleService) { }

  ngOnInit() {
    this.headerPage.title.subscribe(newTitle => {
      this.title = newTitle;
    })
    this.headerPage.link.subscribe(newLink => {
      this.link = newLink;
    })
  }

}
