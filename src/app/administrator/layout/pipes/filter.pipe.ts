import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(noms: any, terme: any): any {
    
    if(terme===undefined) return noms;
    return noms.filter(function(nom){
      return nom.last_name.toLowerCase().includes(terme.toLowerCase());
    })
  }

}
