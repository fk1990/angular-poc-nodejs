import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { AllFormationsComponent } from "./all-formations/all-formations.component";
import { AddFormationComponent } from "./add-formation/add-formation.component";
import { EditFormationComponent } from "./edit-formation/edit-formation.component";
import { FormationComponent } from "./formation/formation.component";


const routes : Routes = [
        {path : "", component: AllFormationsComponent},
        {path:"add",component:AddFormationComponent},
        {path:"edit/:id",component:EditFormationComponent},
        {path:"detail/:id", component:FormationComponent},]


@NgModule({
    imports: [CommonModule,RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class FormationRoutingModule{
    
}