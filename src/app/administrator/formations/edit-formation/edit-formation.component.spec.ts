import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFormationComponent } from './edit-formation.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormationsService } from 'src/app/services/formations.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormationComponent } from '../formation/formation.component';
import { HttpClientModule } from '@angular/common/http';

describe('EditFormationComponent', () => {
  let component: EditFormationComponent;
  let fixture: ComponentFixture<EditFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFormationComponent, FormationComponent ],
      imports: [ReactiveFormsModule, RouterTestingModule, HttpClientModule],
      providers : [FormationsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
