import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Formation } from '../Formation';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { FormationsService } from 'src/app/services/formations.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-formation',
  templateUrl: './edit-formation.component.html',
  styleUrls: ['./edit-formation.component.scss']
})
export class EditFormationComponent implements OnInit {
  @Input() formation =null;
  imageFormation: File;
  @ViewChild("imgFormation")  img_Formation;
  formUpdate : FormGroup;
  id : number;
  constructor(private fb: FormBuilder, private update : FormationsService, private routeId : ActivatedRoute) {
      this.routeId.params.subscribe(params => this.id = params['id']);
   }

  ngOnInit() {
    //this.update.getOneFormation(this.id).subscribe((dataOnId)=>{})
     if(this.formation){
      this.formUpdate = this.fb.group({
        nom : [this.formation.name_formation,Validators.compose([Validators.required,Validators.minLength(3)])],
        description : [this.formation.descript_formation,Validators.compose([Validators.required, Validators.minLength[120],Validators.maxLength[160]])],
        ens : [this.formation.ens_formation,Validators.compose([Validators.required,Validators.minLength[3],Validators.pattern['^[a-zA-Z \-\']+']])],
        file : ["",Validators.required]
        
      })
     }
 
  }
  get form(){
    return this.formUpdate.controls;
  }
  updateUser(){
    if(this.formUpdate.value.file){
      var path = this.formUpdate.value.file;
      console.log(path)
      var filename = path.replace(/^.*\\/, "");
      this.formUpdate.value.file = filename;
      this.toUploadFile();
    }
    this.formUpdate.value.id_formation = this.id;
    console.log(this.formUpdate.value);
    this.update.onUpdateFormation(this.formUpdate.value).subscribe(data => console.log(data))

  }
  toUploadFile(){
    const img = this.img_Formation.nativeElement;
    if(img.files && img.files[0]){
      this.imageFormation = img.files[0]
    }
    const ImageFile : File = this.imageFormation;
    const fd : FormData = new FormData();
    fd.append('file', ImageFile, ImageFile.name);
    this.update.uploadFormationImages(fd).subscribe(
    (data) => console.log(data));
  }

}
