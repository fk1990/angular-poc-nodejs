export class Formation {
    public id_formation:number;
    public name_formation: string;
    public descrip_formation: string;
    public ens_formation: string;
    public img_formation : string;
}