import { NgModule } from "@angular/core";
import { DeleteFormationComponent } from "./delete-formation/delete-formation.component";
import { FormationComponent } from "./formation/formation.component";
import { EditFormationComponent } from "./edit-formation/edit-formation.component";
import { AllFormationsComponent } from "./all-formations/all-formations.component";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { AddFormationComponent } from "./add-formation/add-formation.component";
import { FormationRoutingModule } from "./formation-routing.module";
import { FormationsComponent } from './formations/formations.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';


@NgModule({
    declarations : [
        FormationComponent,
        EditFormationComponent,
        AllFormationsComponent,
        AddFormationComponent,
        FormationsComponent,

    ],
    imports : [
        CommonModule,
        ReactiveFormsModule,
        FormationRoutingModule,
        PaginationModule
    ]
})
export class FormationModule{

}