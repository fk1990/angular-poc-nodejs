import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFormationComponent } from './delete-formation.component';
import { FormationsService } from 'src/app/services/formations.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';


describe('DeleteFormationComponent', () => {
  let component: DeleteFormationComponent;
  let fixture: ComponentFixture<DeleteFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteFormationComponent ],
      imports : [HttpClientModule],
      providers : [FormationsService,{provide : BsModalRef}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
