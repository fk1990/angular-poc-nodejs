import { Component, OnInit } from '@angular/core';

import { FormationsService } from 'src/app/services/formations.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Formation } from '../Formation';

@Component({
  selector: 'app-delete-formation',
  template: `<div class="modal-body text-center" >
  <div class="alert alert-success" *ngIf="deleted">
    utilisateur supprimer avec succès 
  </div>
<p >Etes vous sur de supprimer un utilisateur</p>
<button type="button" class="btn btn-default" (click)="confirm(1)" >Yes</button>
<button type="button" class="btn btn-primary" (click)="decline()" >No</button>
</div>`,
  styleUrls: ['./delete-formation.component.scss']
})
export class DeleteFormationComponent implements OnInit {
  deleted  = false;
  formation : Formation[];
  constructor(private deleteService : FormationsService, private modal : BsModalRef) { }

  ngOnInit() {
    //this.refreshformation();
  }

  confirm(id){
    this.deleteService.onDeleteFormation(id).subscribe(
      (data) => {
        console.log(data);
        
      }
    )
    this.refreshformation();
    this.modal.hide();
  }
  refreshformation(){
    this.deleteService.getAllFormations().subscribe(
      data => {
        this.formation=data;
      }
      
    )
  }
  decline(){
    this.modal.hide()
  }
}
