import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFormationComponent } from './add-formation.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormationsService } from 'src/app/services/formations.service';

describe('AddFormationComponent', () => {
  let component: AddFormationComponent;
  let fixture: ComponentFixture<AddFormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFormationComponent ],
      imports: [ReactiveFormsModule],
      providers : [{provide : FormationsService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
