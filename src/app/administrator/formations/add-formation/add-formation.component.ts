import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormationsService } from 'src/app/services/formations.service';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-add-formation',
  templateUrl: './add-formation.component.html',
  styleUrls: ['./add-formation.component.scss']
})
export class AddFormationComponent implements OnInit {
    formFormation : FormGroup;
    imageFormation: File;
    @ViewChild("imgFormation")  img_Formation;
  constructor(private fb : FormBuilder, private addFormation : FormationsService, private headerService : TitleService) { }

  ngOnInit() {
    this.setTitleLink("Ajouter une formation", "formations")
    this.formFormation = this.fb.group({
      nom : ['',Validators.compose([Validators.required,Validators.minLength(3)])],
      description : ['',Validators.compose([Validators.required, Validators.minLength[120],Validators.maxLength[160]])],
      ens : ['',Validators.compose([Validators.required,Validators.minLength[3],Validators.pattern['^[a-zA-Z \-\']+']])],
      file : ['',Validators.required]
    })
  }

  get form(){
    return this.formFormation.controls;
  }

  createFormation(){
    var path = this.formFormation.value.file;
    var filename = path.replace(/^.*\\/, "");
    this.formFormation.value.file = filename;
    this.toUploadFile();
    this.addFormation.onCreateFormation(this.formFormation.value).subscribe(data => console.log(data));
  }

  toUploadFile(){
    const img = this.img_Formation.nativeElement;
    if(img.files && img.files[0]){
      this.imageFormation = img.files[0]
    }
    const ImageFile : File = this.imageFormation;
    const fd : FormData = new FormData();
   fd.append('file', ImageFile, ImageFile.name);
    this.addFormation.uploadFormationImages(fd).subscribe(
    (data) => console.log(data));
  }
  setTitleLink(title, link){
    this.headerService.setTitle(title);
    this.headerService.setLink(link)
  }
}
