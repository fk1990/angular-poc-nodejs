import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formations',
  template: `<div class="app-formation"><router-outlet></router-outlet></div>`
})
export class FormationsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
