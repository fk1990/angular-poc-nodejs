import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllFormationsComponent } from './all-formations.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('AllFormationsComponent', () => {
  let component: AllFormationsComponent;
  let fixture: ComponentFixture<AllFormationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllFormationsComponent ],
      imports : [RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllFormationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

});
