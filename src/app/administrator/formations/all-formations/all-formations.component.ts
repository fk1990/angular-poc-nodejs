import { Component, OnInit } from '@angular/core';
import { FormationsService } from 'src/app/services/formations.service';

import { Formation } from '../Formation';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DeleteFormationComponent } from '../delete-formation/delete-formation.component';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-all-formations',
  templateUrl: './all-formations.component.html',
  styleUrls: ['./all-formations.component.scss']
})
export class AllFormationsComponent implements OnInit {
  formations : Formation[];
  modalRef : BsModalRef;
  constructor(private allformations: FormationsService, private modal : BsModalService, private headerService : TitleService) { }

  ngOnInit() {
    this.allformations.getAllFormations().subscribe(
      (allFormations : Formation[])=>{
        this.formations = allFormations
      });
      this.setTitleLink("Les formation","formations/add")
  }
  onDeleteFormation(formation){
    const id = formation.id_formation;
    this.modalRef = this.modal.show(DeleteFormationComponent,{initialState :{
      id : id,
      //name : formation.name_formation

    }});
    this.modalRef.content.closeBtnName = 'Close';
  }
  setTitleLink(title, link){
    this.headerService.setTitle(title);
    this.headerService.setLink(link)
  }
}
