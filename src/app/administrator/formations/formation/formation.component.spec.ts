import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationComponent } from './formation.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditFormationComponent } from '../edit-formation/edit-formation.component';
import { FormationsService } from 'src/app/services/formations.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('FormationComponent', () => {
  let component: FormationComponent;
  let fixture: ComponentFixture<FormationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormationComponent, EditFormationComponent ],
      imports : [ReactiveFormsModule, RouterTestingModule,HttpClientModule],
      providers : [FormationsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
