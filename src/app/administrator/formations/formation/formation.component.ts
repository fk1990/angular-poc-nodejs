import { Component, OnInit, Input } from '@angular/core';
import { FormationsService } from 'src/app/services/formations.service';
import { ActivatedRoute } from '@angular/router';
import { Formation } from '../Formation';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.scss']
})
export class FormationComponent implements OnInit {
    id_formation : number = null;
    @Input() formation: Formation = null;
    constructor(private oneFormation:FormationsService, private router : ActivatedRoute, private headerService : TitleService) {
      this.router.params.subscribe(params=> this.id_formation = params["id"]);
    }

  ngOnInit() {
 
    this.oneFormation.getOneFormation(this.id_formation).subscribe((data : Formation)=>{
      this.formation = data;
      console.log(this.formation);
     this.setTitleLink(this.formation.name_formation, "formations");
    })
  }
  setTitleLink(title, link){
    this.headerService.setTitle(title);
    this.headerService.setLink(link)
  }

}
