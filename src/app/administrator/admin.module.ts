import { NgModule } from "@angular/core";
import { AdminComponent } from "./admin.component";
import { AdminRoutingModule } from "./admin-routing.module";


import { CommonModule } from "@angular/common";
import { DeleteUserComponent } from "./users/delete-user/delete-user.component";
import { AlertComponent } from './layout/alert/alert.component';
import { TitleComponent } from './layout/title/title.component';
import { DashboardComponent } from "./dashboard/dashboard.component";
import { TopMenuComponent } from "./layout/top-menu/top-menu.component";
import { SideMenuComponent } from "./layout/side-menu/side-menu.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilterPipe } from './layout/pipes/filter.pipe';




@NgModule({
    declarations : [
        AdminComponent,
        AlertComponent,
        TitleComponent,
        TopMenuComponent,
        SideMenuComponent,
    ],
    imports:[
        CommonModule,
        AdminRoutingModule,
        BrowserAnimationsModule
       
    
    ]
})
export class AdminModule{

}