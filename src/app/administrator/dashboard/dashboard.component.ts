import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import {Chart} from 'chart.js';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
  public doughnutChartData:number[] = [350, 450, 100];
  public doughnutChartType:string = 'doughnut';
  constructor(private headerService : TitleService, private _dashboardService : DashboardService) {

   
   }
  dataUser : Object;
  chart = [];
  ngOnInit() {
    this.setTitle("Tableau de bord")
    this.setLink("");
    this.setUser();
  
  }
  

  setTitle(title){
    this.headerService.setTitle(title);
  }
  setLink(link){
    this.headerService.setLink(link)
  }
  setUser(){
    this._dashboardService.dashUser().subscribe(data =>{
      this.dataUser = data;
      let users = data['users'];
     this.chart = new Chart('canvas', {
        type : 'doughnut',
        data : {
          datasets: [{
              data: [users],
              backgroundColor : "rgb(0, 123, 255)",
          }],
          labels : ["Users"]
      },
        options : {
          legend : {
            display : false
          }
        }
      })
      
    })
  }
  
}
