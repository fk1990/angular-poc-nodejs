import { OnInit, Component } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { AlertService } from "../services/alert.service";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";


@Component({
    selector : "app-admin",
    templateUrl : './admin.component.html'
})
export class AdminComponent implements OnInit{
      title : string = null;
      admin : Object;
       message: any;
      subscription: Subscription;
      constructor(private auth : AuthService, private alertService : AlertService, private router : Router){
        this.subscription = this.alertService.getMessage().subscribe(msg => {this.message = msg; console.log("azaz ", this.message)});

      }
    ngOnInit(){

      let id = localStorage.getItem("id_admin");
      this.auth.getAdmin(id).subscribe(res=>{
        this.admin = res;
      },err => {
        console.log("my error", err);
      });
      //this.router.navigate(["/admin/dashboard"])
    }
    ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.subscription.unsubscribe();
  }

}