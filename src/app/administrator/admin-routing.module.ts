import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AdminComponent } from "./admin.component";
import { AuthGuard } from "../services/auth.guard";
import { DashboardComponent } from "./dashboard/dashboard.component";



const routes : Routes = [
    {path : "admin", component: AdminComponent, children : [
        {path: "", redirectTo : "dashboard", pathMatch : "full"},
        {path: "dashboard", loadChildren : "./dashboard/dashboard.module#DashboardModule"},
        {path: "formations", loadChildren : "./formations/formation.module#FormationModule"},
        {path: "users", loadChildren : "./users/user.module#UserModule"},
        {path:"subscribes",loadChildren : "./formationsUsers/subscribe.module#SubscribeModule"}
   ]},
   //{path : "**", component: DashboardComponent}
]


@NgModule({
    imports : [CommonModule, RouterModule.forChild(routes)],
    exports : [RouterModule]

})
export class AdminRoutingModule{

}