import { Component, OnInit } from '@angular/core';
import { Subscribe } from '../subscribe';
import { SubscribesService } from 'src/app/services/subscribes.service';
import { ActivatedRoute } from '@angular/router';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-detail-subscribe',
  templateUrl: './detail-subscribe.component.html',
  styleUrls: ['./detail-subscribe.component.scss']
})
export class DetailSubscribeComponent implements OnInit {
  subscribe : Subscribe;
  id : string;
  constructor(private subs: SubscribesService, private route : ActivatedRoute, private titleService : TitleService) { 
    this.route.params.subscribe((params)=>{this.id = params['id']})
  }

  ngOnInit() {
  this.subs.getSubscriber(this.id).subscribe((res)=>{
      this.subscribe = res
      this.setTitleLink(this.subscribe.name_formation, "subscribes");
    })
   

  }
  setTitleLink(title, link){
    this.titleService.setTitle(title);
    this.titleService.setLink(link)
  }
}
