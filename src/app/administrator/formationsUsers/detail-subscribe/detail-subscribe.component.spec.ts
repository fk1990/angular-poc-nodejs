import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSubscribeComponent } from './detail-subscribe.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SubscribesService } from 'src/app/services/subscribes.service';
import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/compiler/src/core';

describe('DetailSubscribeComponent', () => {
  let component: DetailSubscribeComponent;
  let fixture: ComponentFixture<DetailSubscribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSubscribeComponent ],
      imports : [RouterTestingModule,HttpClientModule, RouterTestingModule],
      providers : [SubscribesService],
     
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSubscribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
