import { Component, OnInit } from '@angular/core';
import { SubscribesService } from 'src/app/services/subscribes.service';
import { Subscribe } from '../subscribe';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-formations-users',
  templateUrl: './formations-users.component.html',
  styleUrls: ['./formations-users.component.scss']
})
export class FormationsUsersComponent implements OnInit {
  subscribers : Subscribe[];
  constructor(private subscribes : SubscribesService, private titleService : TitleService) { }

  ngOnInit() {
    this.subscribes.getSubscribers().subscribe((res)=>{
      this.subscribers = res;
    });
    this.setTitleLink("Les formations organisées", "");
  }

  setTitleLink(title, link){
    this.titleService.setTitle(title);
    this.titleService.setLink(link)
  }


}
