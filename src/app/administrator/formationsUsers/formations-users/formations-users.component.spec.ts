import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationsUsersComponent } from './formations-users.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { SubscribesService } from 'src/app/services/subscribes.service';

describe('FormationsUsersComponent', () => {
  let component: FormationsUsersComponent;
  let fixture: ComponentFixture<FormationsUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormationsUsersComponent ],
      imports : [RouterTestingModule, HttpClientModule],
      providers :[SubscribesService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormationsUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
