import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFormationUserComponent } from './add-formation-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SubscribesService } from 'src/app/services/subscribes.service';
import { UsersService } from 'src/app/services/users.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormationsService } from 'src/app/services/formations.service';
import { HttpClientModule } from '@angular/common/http';

describe('AddFormationUserComponent', () => {
  let component: AddFormationUserComponent;
  let fixture: ComponentFixture<AddFormationUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFormationUserComponent ],
      imports : [ReactiveFormsModule, BsDatepickerModule, RouterTestingModule, HttpClientModule],
      providers : [SubscribesService,UsersService,FormationsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFormationUserComponent);
    const getOneFormation = null;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    let allUsers = component.allUsers;
    allUsers = null;
  });
});
