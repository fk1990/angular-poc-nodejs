import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from '../../users/user';
import { ActivatedRoute, Router } from '@angular/router';
import { FormationsService } from 'src/app/services/formations.service';
import { Formation } from '../../formations/Formation';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubscribesService } from 'src/app/services/subscribes.service';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-add-formation-user',
  templateUrl: './add-formation-user.component.html',
  styleUrls: ['./add-formation-user.component.scss']
})
export class AddFormationUserComponent implements OnInit {
    users : User[] = null;
    idFormation: number = null;
    oneFormation : Formation;
    formSubscribe : FormGroup;
    userId: number = null;
    minDate : Date;
    user : User;
    selectedIndex : number = null;
    constructor(private usersService : UsersService, private route : ActivatedRoute, private formation : FormationsService, private fb: FormBuilder, private subscribe : SubscribesService, private titleService : TitleService, private router : Router) { 
      this.route.params.subscribe(params => this.idFormation = params['id'])
    }

  ngOnInit() {
    this.setTitleLink("S'abonner à une formation", "subscribes");
    this.allUsers();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() + 1);
    this.formSubscribe = this.fb.group({
      userId : [],
      idFormation : [],
      date:['',Validators.required]
    });
    this.fetchFormation();
   
  }
  onSelect(id, index){
    this.userId = id;
    this.fetchUser();
    this.selectedIndex = index;
    
  }
  fetchFormation(){
      this.formation.getOneFormation(this.idFormation).subscribe((res)=>{
        this.oneFormation = res;
 
      });
      
  }
  fetchUser(){
    this.usersService.detailUser(this.userId).subscribe((res)=>{
      this.user = res;
    })
  }
  allUsers(){
    this.usersService.allUsers().subscribe((res)=>{
      this.users = res;
    });
  }
  createSubscribe(value){
    if(!this.formSubscribe.invalid){
      this.subscribe.addSubscriber(value).subscribe(res =>{
          this.formSubscribe.reset;
          this.router.navigate(['/admin/subscribes']);
          
      });
    }
    

  }
  setTitleLink(title, link){
    this.titleService.setTitle(title);
    this.titleService.setLink(link);
  }
}
