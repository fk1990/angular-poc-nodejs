import { Routes, RouterModule } from "@angular/router";
import { FormationsUsersComponent } from "./formations-users/formations-users.component";
import { AddFormationUserComponent } from "./add-formation-user/add-formation-user.component";
import { DetailSubscribeComponent } from "./detail-subscribe/detail-subscribe.component";
import { NgModule } from "@angular/core";



const routes : Routes = [
        {path:"", component:FormationsUsersComponent},
        {path : "add/:id",component:AddFormationUserComponent},
        {path : "detail/:id",component:DetailSubscribeComponent}
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class SubscribeRoutingModule{

}