import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFormationUserComponent } from './delete-formation-user.component';

describe('DeleteFormationUserComponent', () => {
  let component: DeleteFormationUserComponent;
  let fixture: ComponentFixture<DeleteFormationUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteFormationUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFormationUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
