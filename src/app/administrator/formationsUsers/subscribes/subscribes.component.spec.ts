import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribesComponent } from './subscribes.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('SubscribesComponent', () => {
  let component: SubscribesComponent;
  let fixture: ComponentFixture<SubscribesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscribesComponent ],
      imports : [RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
