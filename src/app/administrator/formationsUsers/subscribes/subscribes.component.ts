import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subscribes',
  template: `<div class="app-formation"><router-outlet></router-outlet></div>`,
})
export class SubscribesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
