import { NgModule } from "@angular/core";
import { FormationsUsersComponent } from "./formations-users/formations-users.component";
import { AddFormationUserComponent } from "./add-formation-user/add-formation-user.component";
import { DeleteFormationUserComponent } from "./delete-formation-user/delete-formation-user.component";
import { DetailSubscribeComponent } from "./detail-subscribe/detail-subscribe.component";
import { SubscribeRoutingModule } from "./subscribe-routing.module";
import { CommonModule } from "@angular/common";
import { SubscribesComponent } from "./subscribes/subscribes.component";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ReactiveFormsModule } from "@angular/forms";
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';





@NgModule({
    declarations : [
        FormationsUsersComponent,
        AddFormationUserComponent,
        DeleteFormationUserComponent,
        DetailSubscribeComponent,
        SubscribesComponent

    ],
    imports : [
        CommonModule,
        SubscribeRoutingModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot()
        
        
    ]
})
export class SubscribeModule{

}