import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import { LoginComponent } from './home/login/login.component';
import { AuthGuard } from './services/auth.guard';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { AdminModule } from './administrator/admin.module';
import { DeleteUserComponent } from './administrator/users/delete-user/delete-user.component';
import { DeleteFormationComponent } from './administrator/formations/delete-formation/delete-formation.component';





@NgModule({
  entryComponents : [DeleteUserComponent, DeleteFormationComponent],
  declarations: [
    AppComponent,
    LoginComponent,
    DeleteUserComponent,
    DeleteFormationComponent

  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AdminModule,


 
    
  ],
  providers: [AuthGuard, {provide : HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
