const express = require("express");
const router = express.Router();
const db = require("../database/db");
const jwt = require('jsonwebtoken');


// START CRUD USERS
// all users
router.get('/', verifyToken,function(req,res){
    var query = "SELECT * FROM users";
    db.query(query,function(err,result,fields){
        if(err) throw err;
        res.status(201).send(result);
    })
});
router.post('/grouped',function(req,res){
    var query = `SELECT * FROM users LIMIT ${req.body.from},${req.body.to}` ;
    db.query(query,function(err,result,fields){
        if(err) throw err;
        console.log(result);
        res.status(201).send(result);
    })
});

// detail user
router.get('/:id',function(req,res){
    let query = `SELECT * FROM users WHERE id_users = ${req.params.id}`;
    db.query(query,function(err,result,fields){
        if(err) throw err;
        res.send(result[0]);
    })
})

// add user
router.post('/add',function(req,res){
    var data = req.body;
    let query = `INSERT INTO  
            users (id_users,last_name,first_name,email,password,website,phone,poste)
            VALUES (0,'${data.nom}','${data.prenom}','${data.email}','${data.password}','${data.website}','${data.phone}','${data.poste}');`
   db.query(query,data,function(err,result,fields){
        if(err) throw err;
        res.status(201).send(result);
        console.log('record inserted');
    })
})

// delete user
router.delete('/delete/:id',(req, res)=>{
    let id = req.params.id;
    let query = `DELETE FROM users WHERE id_users = ${id}`;
    db.query(query, (err, result)=>{
        if(err) throw err
        res.status(200).send(result);
        console.log(result, " est supprimé")

    })
})

//update user
router.put('/edit/:id',function(req,res){
    let query = `UPDATE users SET ? WHERE id_users= ${req.params.id}`;
    db.query(query,req.body,function(err, result){
        if(err) throw err;
        console.log(result + " record(s) updated");
        res.status(200).send(result);
    });
});
// END CRUD USERS


//JWT AUTHENTIFICATION

router.post('/login',(req,res)=>{
    let data = req.body;
    let query = `SELECT id_admin,email, password FROM admin WHERE email= '${data.email}' AND password = '${data.password}' LIMIT 1`;
    db.query(query,function(err, result){
        if(err) throw err;
        if(result.length!=0){
            let payload = {subject : `${result[0].id_admin}`};
            let token = jwt.sign(payload, 'secretKey');
            res.status(200).send({token, "adminId" : result[0].id_admin}); 
        }else{
            res.status('401').send("Mot de passe et/ou email incorrecte");
        }
        

    });
});
function verifyToken(req, res, next){
    let token = req.headers.authorization.split(' ')[1];
    if(!req.headers.authorization){
        return res.status(401).send('Unauthorized request')
    }
   
    if(token === 'null'){
        return res.status(401).send('Unauthorized request');
    }
   
    jwt.verify(token, 'secretKey', function(err, result){
        if(err){
            throw err;
        }else{


        }
    });

    /*if(!payload){
        return res.status(401).send('Unauthorized request');
    }
    req.result[0] = payload.subject;*/
    next();
}

module.exports = router;