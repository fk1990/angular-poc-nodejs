const express = require("express");
const router = express.Router();
const db = require("../database/db");


router.get("/", (req, res)=>{
    let query = 
    `SELECT COUNT(formations_users.idformations_users) AS num_users, idformations_users,formations.id_formation, formations.name_formation, date FROM formations_users, formations,users 
    WHERE formations.id_formation = formations_users.id_formation GROUP BY formations_users.id_users
    AND users.id_users = formations_users.id_users`;
    db.query(query,(err,result,field)=>{
        if (err) throw err;
        res.status(200).send(result);
    });
})
router.get("/:id", (req, res)=>{
    let id = req.params.id;
    let query = 
    `SELECT COUNT(formations_users.id_users) AS num_users,idformations_users,formations.id_formation, formations.name_formation, 
    date FROM formations_users, formations,users 
    WHERE formations.id_formation = formations_users.id_formation
    AND users.id_users = formations_users.id_users AND formations.id_formation= ${id}`;
    db.query(query,(err,result,field)=>{
        if (err) throw err;
        res.status(200).send(result[0]);
    });
})

router.post('/add',(req,res)=>{
    console.log(req.body);
   let query = `INSERT INTO formations_users 
    (idformations_users, id_users, id_formation, date) VALUES 
    (null, '${req.body.userId}', '${req.body.idFormation}', '${req.body.date}');`
   db.query(query,(err,result)=>{
        if(err) throw err;
        res.status(201).send(result);
        console.log("abonnement fait");
    })
})

module.exports = router;