const express = require("express");
const router = express.Router();
const db = require("../database/db");
router.get('/users', (req, res)=>{
    let query = `SELECT COUNT(id_users) AS users From users`;
    db.query(query, (err,result,fields)=>{
        if(err) throw err;
        res.status(200).send(result[0]);
    })
})

module.exports = router;