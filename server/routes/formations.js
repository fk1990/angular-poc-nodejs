const express = require("express");
const router = express.Router();
const multer = require("multer");
const URL = '../formations/src/';
const db = require('../database/db');
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth();
var year = today.getFullYear();
var hoy = dd+"-"+mm+"-"+year;


router.get('/',(req, res)=>{
    let query = "SELECT * FROM formations";
    db.query(query,function(err,result,fields){
        if(err) throw err;
        res.status(200).send(result);
    })
    
});
router.get('/:id',function(req,res){
    let query = `SELECT * FROM formations WHERE id_formation = ${req.params.id}`;
    db.query(query,function(err,result,fields){
        if(err) throw err;
        console.log(result);
        res.status(200).send(result[0]);
    })
})
router.post('/add',function(req,res){
    var data = req.body;
    var date = hoy+"."+ data.file;
    let query = `
    INSERT INTO formations (id_formation,name_formation,descript_formation,ens_formation,img_formation) 
    VALUES (0, '${data.nom}', '${data.description}', '${data.ens}','${date}')`;
    db.query(query,function(err,result,fields){
        if(err) throw err;
        res.status(200).send(result);
    });
});
var store = multer.diskStorage({
    destination : function(req,file,cb){
        cb(null, URL + 'assets/uploads')
    },
    filename: function(req, file, cb){
        cb(null,hoy+'.'+file.originalname)
    }
});
var upload = multer({storage: store}).single('file');

router.post('/file',function(req, res){
    upload(req, res,function(err){
        if(err) throw err
        return res.json();
    });
})

router.delete('/delete/:id',function(req,res){
    var data = req.body;
    console.log(req.body);
    let query = `DELETE FROM formations WHERE id_formation = ${req.params.id}`;
    db.query(query,data,function(err,result,fields){
        if(err) throw err;
        res.status(200).send(result);
        console.log('record deleted');
    });
});
router.put('/edit/:id',function(req,res){
    let date = hoy+"."+req.body.file
    let query = `UPDATE formations SET name_formation ='${req.body.nom}', ens_formation = '${req.body.ens}', 
    img_formation = '${date}', descript_formation= '${req.body.description}'
    WHERE id_formation = '${req.body.id_formation}'`;
    db.query(query,function(err,result,fields){
            if(err) throw err;
            res.status(200).send(result);
            console.log('record updated');
        });
});


module.exports = router;