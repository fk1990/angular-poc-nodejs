
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const formations = require('./routes/formations');
const users = require('./routes/users');
const subscribes = require('./routes/subscribes');
const admins = require('./routes/admins');
const dashboard = require("./routes/dashboard");
const db = require('./database/db');


// TEST DATABASE
db.connect((error)=>{
    if (error) throw error;
});

app.get("/", (req, res)=>{
    res.status(200).send();
});

// CONFIG APP
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json());
// ROUTES
app.use('/formations',formations);
app.use('/users', users)
app.use('/subscribes', subscribes)
app.use('/admins',admins)
app.use('/dashboard',dashboard)
// LOCALHOST:9000
app.listen(9000);